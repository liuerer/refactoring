package com.twu.refactoring;

/**
 * OrderReceipt prints the details of order including customer name, address, description, quantity,
 * price and amount. It also calculates the sales tax @ 10% and prints as part
 * of order. It computes the total order amount (amount of individual lineItems +
 * total sales tax) and prints it.
 */
public class OrderReceipt {
	private Order order;
	
	public OrderReceipt(Order order) {
		this.order = order;
	}
	
	public String printReceipt() {
		String PRINT_HEADERS = "======Printing Orders======";
		
		return PRINT_HEADERS + "\n" +
			order.getCustomerName() +
			order.getCustomerAddress() +
			PrintsLineItems() +
			"Sales Tax" + '\t' + getTotalSalesTax() +
			"Total Amount" + '\t' + getTotalAmount();
	}
	
	private String PrintsLineItems() {
		StringBuilder stringBuilder = new StringBuilder();
		for (LineItem lineItem : order.getLineItems()) {
			stringBuilder.append(lineItem.getDescription()).append('\t');
			stringBuilder.append(lineItem.getPrice()).append('\t');
			stringBuilder.append(lineItem.getQuantity()).append('\t');
			stringBuilder.append(lineItem.totalAmount()).append('\n');
		}
		return stringBuilder.toString();
	}
	
	private String getTotalSalesTax() {
		return String.valueOf(order.getLineItems().stream().mapToDouble(lineItem -> lineItem.totalAmount() * .10).sum());
	}
	
	private String getTotalAmount() {
		return String.valueOf(order.getLineItems().stream().mapToDouble(lineItem -> lineItem.totalAmount() * 1.10).sum());
	}
	
	
}
